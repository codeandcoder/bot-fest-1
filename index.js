
let Bot = require('./bot.js').Bot

let bots = []
bots.push(['Random Bot', require('./bots/random-bot').bot])
bots.push(['Kamikaze Bot', require('./bots/kamikaze-bot').bot])
bots.push(['Regresion Bot', require('./bots/regresion-bot').bot])
bots.push(['Cautious Bot', require('./bots/cautious-bot').bot])
bots.push(['Pinza Bot', require('./bots/pinza-bot').bot])
bots.push(['Locura Bot', require('./bots/locura-bot').bot])
bots.push(['Sesenta% Bot', require('./bots/un-sexto-bot').bot])
bots.push(['Median Bot', require('./bots/median-bot').bot])
bots.push(['Median Bot v2', require('./bots/median-bot-v2').bot])
bots.push(['Harmonic Bot', require('./bots/harmonic-bot').bot])
bots.push(['Random God Bot', require('./bots/random-god-bot').bot])
bots.push(['Copycat Bot', require('./bots/copycat-bot').bot])

let logs = false
let repetitions = 10000
let maxHealth = 100

let stats = championship(bots, repetitions, maxHealth)
let ranking = []

for (let i = Object.keys(stats).length - 1; i >= 0; i--) {
	let botName = Object.keys(stats)[i]
	stats[botName].points = (stats[botName].points / repetitions * 100).toFixed(2) + '%'
	for (var j = Object.keys(stats[botName].roundsLost).length - 1; j >= 0; j--) {
		let round = Object.keys(stats[botName].roundsLost)[j]
		stats[botName].roundsLost[round] = (stats[botName].roundsLost[round] / stats[botName].timesLost * 100).toFixed(2) + '%'
	}
	let killerList = []
	for (var j = Object.keys(stats[botName].killers).length - 1; j >= 0; j--) {
		let k = Object.keys(stats[botName].killers)[j]
		killerList.push({botName: k, times: (stats[botName].killers[k] / stats[botName].timesLost * 100).toFixed(2) + '%'})
	}
	killerList.sort(function compare(a,b) {
		let aPoints = parseFloat(a.times.substring(0,a.times.indexOf('%')))
		let bPoints = parseFloat(b.times.substring(0,b.times.indexOf('%')))
		if (aPoints > bPoints)
			return -1;
		if (aPoints < bPoints)
		    return 1;
		return 0;
	})
	stats[botName].killers = killerList
	ranking.push({botName: botName, points: stats[botName].points})
}
ranking.sort(function compare(a,b) {
	let aPoints = parseFloat(a.points.substring(0,a.points.indexOf('%')))
	let bPoints = parseFloat(b.points.substring(0,b.points.indexOf('%')))
  if (aPoints > bPoints)
    return -1;
  if (aPoints < bPoints)
    return 1;
  return 0;
})

console.log(ranking)
console.log(JSON.stringify(stats))

function championship(bots, reps, initialHp) {
	let stats = {}
	initStats(bots);

	for (var i = reps - 1; i >= 0; i--) {
		let faceOffResult = faceOff(bots, initialHp)
		if (logs) console.log(faceOffResult.winners)
		if (faceOffResult.winners.length > 1) {
			updateStats(faceOffResult.winners[0], false, false)
			updateStats(faceOffResult.winners[1], false, false)
		} else {
			updateStats(faceOffResult.winners[0], true, false)
		}

		for (var j = faceOffResult.losers.length - 1; j >= 0; j--) {
			updateStats(faceOffResult.losers[j], false, true)
		}
		if (logs) console.log('----------------------------------------------------------')
	}

	return stats

	function initStats(bots) {
		for (var i = bots.length - 1; i >= 0; i--) {
			let bot = bots[i]
			stats[bot[0]] = {
				wins : 0,
				points : 0,
				history: {
					timesArrived: [],
					average: []
				},
				timesLost: 0,
				roundsLost: {},
				killers: {}
			}
		}
	}

	function updateStats(bot, soleWinner, lost) {
		let averageHistory = calculateAverageHistory(bot)

		let killers = stats[bot.botName].killers
		let timesLost = stats[bot.botName].timesLost

		if (lost || !soleWinner) {
			killers[bot.killer] = killers[bot.killer] != undefined ? killers[bot.killer] + 1 : 1
			timesLost += 1
		}

		if (lost) {
			let roundsLost = stats[bot.botName].roundsLost;
			roundsLost[bot.roundLost] = roundsLost[bot.roundLost] != undefined ? roundsLost[bot.roundLost]+1 : 1
			stats[bot.botName] = {
				wins : stats[bot.botName].wins,
				points : stats[bot.botName].points,
				history : averageHistory,
				timesLost : timesLost,
				roundsLost : roundsLost,
				killers : killers
			}
			
		} else {
			stats[bot.botName] = {
				wins : stats[bot.botName].wins + 1,
				points : soleWinner ? stats[bot.botName].points + 1 : stats[bot.botName].points + 0.5,
				history : averageHistory,
				timesLost : timesLost,
				roundsLost : stats[bot.botName].roundsLost,
				killers : killers
			}
		}
	}

	function calculateAverageHistory(bot) {
		let oldHistory = stats[bot.botName].history
		for (var i = 0; i < bot.history.length; i++) {
			if (oldHistory.timesArrived.length <= i) {
				oldHistory.timesArrived.push(0)
				oldHistory.average.push(0)
			}
			oldHistory.timesArrived[i]++
			oldHistory.average[i] = ((oldHistory.timesArrived[i]-1) * oldHistory.average[i] + bot.history[i]) / oldHistory.timesArrived[i]
		}
		return oldHistory
	}
}

function faceOff(bots, initialHp) {

	// Initialize bots
	let cBots = []
	for (let i = bots.length - 1; i >= 0; i--) {
		let b = bots[i]
		cBots.push(Bot(b[0], initialHp, b[1]))
	}

	let lastTwo = []
	let losers = []
	let round = 1;

	// Championship Loop
	while (cBots.length > 1) {
		cBots = shuffle(cBots);
		for (let i = cBots.length - 1; i >= 0; i-=2) {
			if (i-1 >= 0) {
				let c1 = cBots[i]
				let c2 = cBots[i-1]
				lastTwo = [c1, c2]

				let tied = true;

				for (let tries = 0; tries < 3 && tied; tries++) {
					let c1Damage = Math.abs(c1.process(c2.history, tries, cBots.length, bots.length))
					c1Damage = c1Damage > c1.hp ? c1.hp : c1Damage
					if (logs) console.log(c1.botName + ' inflicts ' + c1Damage + ' points.')
					let c2Damage = Math.abs(c2.process(c1.history, tries, cBots.length, bots.length))
					c2Damage = c2Damage > c2.hp ? c2.hp : c2Damage
					if (logs) console.log(c2.botName + ' inflicts ' + c2Damage + ' points.')

					if (c1Damage > c2Damage) {
						tied = false
						c2.hp = 0
						c1.hp -= c1Damage
						c1.history.push(c1Damage)
						c2.history.push(c2Damage)
						c2.killer = c1.botName
						if (c1Damage >= c1.hp) {
							c1.killer = 'Suicide'
						}
					} else if (c2Damage > c1Damage) {
						tied = false
						c1.hp = 0
						c2.hp -= c2Damage
						c1.history.push(c1Damage)
						c2.history.push(c2Damage)
						c1.killer = c2.botName
						if (c2Damage >= c2.hp) {
							c2.killer = 'Suicide'
						}
					}
				}

				if (tied) {
					c1.hp = 0
					c2.hp = 0
					c1.killer = c2.botName
					c2.killer = c1.botName
				}

				if (cBots.length == 2 && c1.hp == 0 && c2.hp == 0) {
					cBots = []
				} else {
					if (c1.hp <= 0) {
						c1.roundLost = round
						losers.push(cBots.splice(i,1)[0])
						if (logs) console.log(c1.botName + ' dies.')
					}
					if (c2.hp <= 0) {
						c2.roundLost = round
						losers.push(cBots.splice(i-1,1)[0])
						if (logs) console.log(c2.botName + ' dies.')
					}
				}
			}
		}
		if (logs) console.log('-------- ROUND ' + round + ' ENDED')
		round++
	}

	if (cBots.length > 0) {
		lastTwo = [cBots[0]]
	}
	
	return {winners: lastTwo, losers: losers}
}

function shuffle(array) {
  let currentIndex = array.length, temporaryValue, randomIndex

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex)
    currentIndex -= 1

    // And swap it with the current element.
    temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }

  return array
}
