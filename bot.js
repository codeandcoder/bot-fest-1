exports.Bot = function(botName, initialHp, act) {
	let self = {}

	self.botName = botName
	self.hp = initialHp
	self.act = act
	self.history = []
	self.roundLost = -1
	self.killer = ''

	self.process = function(otherBotHistory, ties, botsAlive, botsStart) {
		let result = self.act(self.hp, otherBotHistory, ties, botsAlive, botsStart)
		return result
	}

	return self
}