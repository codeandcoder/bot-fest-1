exports.bot = function(hp, history, ties, alive, start) {
	if (alive == 2) {
		return hp - 1 + ties
	}

	let enemyRemainingLife = 100 - history.reduce((a, b) => a + b, 0)

	let rndNum = random(0.25,0.35)
	return Math.min(((0.632-rndNum)*hp + rndNum*enemyRemainingLife), 0.7 * hp, 0.75*enemyRemainingLife) + ties + 1
}

function random(min, max) {
	return Math.random() * (max - min) + min
}