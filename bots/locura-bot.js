exports.bot = function(hp, history, ties, alive, start) {
	if (alive == 2) {
		return hp - 1 + ties * 2
	}

	let round = history.length
	let enemyRemainingLife = 100 - history.reduce((a, b) => a + b, 0)

	if (enemyRemainingLife < hp)
		return Math.min(enemyRemainingLife + ties, (0.65 + 0.1 * round) * hp)

	return random((0.6 + 0.1 * round) * hp, (0.65 + 0.1 * round) * hp)
}

function random(min, max) {
	return Math.random() * (max - min) + min
}